# Stapelverarbeitung

Die Stapelverarbeitung erlaubt es, viele Aufgaben gleichzeitig zu erstellen, ohne die Metadaten erneut eingeben zu müssen. Dies geht nur für [Eingabe-Aufgaben](aufgabe-bearbeiten.md#aufgabentypen).

## Metadaten

Die Metadaten gellten für alle zu erstellenden Aufgaben.

### Aufgaben-Name

Dies ist der Name der Aufgaben. Bei Vokabelaufgaben könnte dieser beispielsweise `Vokabeln` lauten, oder bei Mathematikaufgaben `Bruchrechnen 1`. Dieser dient nur der Orientierung und muss nicht auf den Inhalt der Aufgaben bezogen sein.

### Themen

Unter den gewählten Themen können die Aufgaben gefunden werden. Wie neue Themen erstellt und verwaltet werden werden, findest du unter [Aufgabe bearbeiten](aufgabe-bearbeiten.md#themen).

### Optionen

Aufgaben können entweder in nur eine Richtung oder in beide Richtungen erstellt werden.

 - In **eine Richtung** sollten Aufgaben zum Beispiel für *Mathematikaufgaben*. Hier soll zu einer Gleichung die Lösung angegeben werden. Es ergäbe jedoch keinen Sinn, zu der Antwort die Gleichung abzufragen
 - In **beide Richtungen** können zum Beispiel *Vokabelaufgaben* erstellt werden. Hier kann sowohl von der Sprache 1 in Sprache 2 wie auch umgekehrt abgefragt werden.

!!! warn
    Es ist wichtig, anzugeben, ob die Aufgaben in beide Richtungen erstellt werden sollen. Andernfalls kann es sein, dass die erstellten Aufgaben keinen Sinn ergeben.

### Lizenz

Beim Speichern einer Aufgabe werden alle Rechte an dieser auf die TestApp übertragen. Wir geben die Aufgaben unter den Bedingungen der [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de) Lizenz weiter.

## Die einzelnen Aufgaben

Für jede einzelne Aufgabe werden im unteren Teil des Fensters die Frage-Antwort-Paare eingegeben.

Sprache 1 (nur zur orientierung bei Vokabelaufgaben, kann auch anders verwendet werden) ist die Aufgabenstellung und Sprache 2 ist die Antwort. Wenn Unter [Optionen](#optionen) beide Richtungen ausgewählt wurden, wird natürlich auch in dem zweiten Durchlauf Sprache 2 als Aufgabenstellung und Sprache 1 als Antwort verwendet.

### RegEx

Wie unter [Aufgabe bearbeiten](aufgabe-bearbeiten.md#aufgabentypen) erklärt, können auch hier RegEx verwendet werden.

Wenn du keine RegEx verwenden möchtest, kannst du die dazugehörigen Eingabefelder einfach leer lassen.

### Eine Aufgabe hinzufügen

Durch Tippen auf `Eine hinzufügen` wird eine leere Aufgaben-Reihe hinzugefügt. Dies geht nur, wenn die Reihe davor nicht leer ist.

## Aufgaben generieren

Durch Tippen `Aufgaben generieren` werden aus den eingegeben Daten letztendlich die Aufgaben generiert. Es erscheint ein Ladebalken, der den Fortschritt anzeigt. Wenn viele Aufgaben gleichzeitig generiert werden, kann dies einige Minuten dauern.