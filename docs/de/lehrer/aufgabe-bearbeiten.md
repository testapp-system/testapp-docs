# Aufgaben erstellen und bearbeiten

## Aufgabenstellung

Zu Oberst kann in dieser Ansicht die Aufgabestellung bearbeitet werden.

 - Der **Aufgaben-Name** gibt den Titel/die Überschrift der Aufgabe an. Dieser ist nicht relevant für die Bewertung und dient nur der Orientierung.
 - Die **Aufgabe** enthält die eigentliche Aufgabenstellung, dir die Schüler*innen bearbeiten sollen.

## Aufgabentypen

Die TestApp unterstützt drei verschiedene Aufgabentypen:

 - Bei **Ankreuz-/Multiple-Choice-Aufgaben** müssen die Schüler\*innen zwischen verschiedenen möglichen Antworten auswählen. Es können mehrere Antworten richtig sein oder auch nur eine Einzige.
 - Bei **Eingabe-Aufgaben** müssen die Schüler\*innen die richtige(n) Antwort(en) selbst eintippen. Es word automatisch erkannt, ob auf Mobilgeräten eine alphanummerische oder eine ausschließliche nummerische Tastatur angezeigt werden sollte. Es gibt folgende arten von Eingaben:
     - eine Einzelne Antwort
     - mehrere Antworten, bei denen die Reihenfolge egal ist
     - mehrere Antworten, deren Reihenfolge entscheidend ist
 - Bei **Eingaben-Aufgaben mit regulären Ausdrücken (RegEx)** kann die Mustersprache RegEx verwendet werden, um verschiedene mögliche Antworten abzudecken. Dies ist häufig für Rechenaufgaben notwendig, um Fließkommazahlen, Rundungen und Brüche abzudecken.
     - *Es gibt die gleichen Unterarten wie bei Eingabe-Aufgaben.*

!!! warn
    Da RegEx schlecht für Menschen lesbar sind, empfehlen wir dir, die RegEx mit dem Open-Source Tool [regex101](https://regex101.com) zu testen. Dort finden sich auch Hilfen für häufige Anwendefälle.

## Antworten

Um die Antworten zu bearbeiten muss zuerst der gewünschte Aufgabentyp ausgewählt werden.

!!! info
    Bei **Eingabeaufgaben** muss unbedingt angegeben werden, **ob die Reihenfolge** der Antworten **entscheidend** ist.

Danach können alle gewollten Antworten bzw. Antwortmöglichkeiten eingegeben werden. Die Eingabefelder, die nicht benötigt werden, können einfach leer gelassen werden.

!!! info
    Bei **Ankreuz-/Multiple-Choise-Aufgaben** muss unbedingt **jede der Antwortmöglichkeiten angekreuzt werden, die richtig ist**.

## Themen

Jede Aufgabe muss mindestens einem Thema zugeordnet. Dazu aus dem Auswahlmenü alle gewollten Themen auswählen.

Um ein Thema wieder von der Aufgabe zu entfernen, einfache auf den Chip tippen, auf dem das Thema steht.

### Themen verwalten

Um ein neues Thema zu erstellen, dass es noch nicht gibt, musst du auf den `+`-Knopf tippen. Es erscheint ein Dialog.

In diesem Dialog muss angegeben werden, wie das neue Thema heißen soll und welchem Fach es angehört. Das Fach wird verwendet, um durch die [Aufgabenverwaltung](aufgaben.md) zu navigieren.

Themen, denen gar keine Aufgaben mehr angehören, werden nach zwei Wochen automatisch gelöscht. Du musst dich darum also nicht selbst kümmern.

## Bilder

Die TestApp erlaubt es, Aufgaben Bilder anzufügen. Diese werden beim Schreiben von Tests bei der Aufgabenstellung angezeigt.

## Aufgabe löschen

Durch Tippen auf das :fa-trash:-Symbol in der oberen rechten Ecke, kann die aktuelle Aufgabe gel&ouml;scht werden.

## Lizenz

Beim Speichern einer Aufgabe werden alle Rechte an dieser auf die TestApp übertragen. Wir geben die Aufgaben unter den Bedingungen der [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de) Lizenz weiter.