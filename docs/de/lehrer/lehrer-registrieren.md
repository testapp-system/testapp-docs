# Als Lehrer\*in anmelden

Damit sich Lehrer\*innen anmelden k&ouml;nnen, ben&ouml;tigen sie einen Voucher. Ein Voucher kann von allen Lehrer\*innen einer Schule ausgestellt werden.

## Einen Voucher erstellen

Um einen Voucher zu erstellen, als angemeldete\*r Lehrer\*in unter `Schulverwaltung` einen Voucher restellen. Weitere Infos dazu unter [Schulverwaltung](schulverwaltung.md) Der dann angezeigt Voucher ist eine Woche lang g&uuml;ltig.

Wenn du einen Voucher erhalten hast, bitte den Anweisungen zum [Registrieren](../allgemein/registrieren.md) folgen.