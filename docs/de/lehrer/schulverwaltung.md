# Schulverwaltung

Damit sich Lehrer neu registrieren können, benötigen sie Voucher. Jeder Lehrer kann diese ausstellen.

## Einen neuen Voucher erstellen

Durch Tippen auf `Neuen Voucher erstellen` wird ein neuerer Voucher erstellt und dieser angezeigt.

## Vorhandene Voucher

Alle derzeit gültigen Voucher werden in der Liste darunter angezeigt. Diese können weitergegeben werden, damit sich Lehrer registrieren werden.

## Gültigkeit

Ein neu erstellter Voucher ist eine Woche lang gültig und wird danach automatisch gelöscht.