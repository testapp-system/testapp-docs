# Was ist die TestApp

Die [TestApp](https://www.testapp.ga/) ist ein Open-Source Bildungsprojekt, dass **&Uuml;bungsaufgaben und Schultests digital** auf Smartphones, Computern etc. bereitstellt. Wir bieten **einache Klassenverwaltung**, **individuelle** Tests und **&Uuml;bungsaufgaben**. Sch&uuml;ler\*innen k&ouml;nnen sich **einzeln** anmelden, aber auch **ganze Kurse oder Bildungeinrichtungen** k&ouml;nnen sich anmelden.

## Ich suche Informationen ...

* [**für Lehrer\*innen**](lehrer/)
* [**für Schüler\*innen**](schüler/)
* [**was die TestApp ist**](https://www.testapp.ga/de/)
* [**für Schulen oder Schulträger**](https://www.testapp.ga/de/#getting-started)
* [**in einem Info-Flyer**](https://www.testapp.ga/wp-content/uploads/2020/05/TestApp-Flyer-Standard.pdf) 

Dies hier ist das Benutzerhandbuch. Das heißt, wir beschränken uns an dieser Stelle darauf, zu beschreiben, wie die TestApp funktioniert. Wenn du einen Überblick oder Eindruck von der TestApp haben möchtest, schaue dir bitte unsere [**Info-Webseite**](https://www.testapp.ga/de/) an.

### Downloads

Du kannst die TestApp auf vielen verschiedenen Geräten verwenden. Hier siehst du, welche Platformen wir alles unterstützen.

* Windows 10: [Microsoft Store](https://www.microsoft.com/en-us/p/testapp-system/9p9jjfmff77d)
* Android:
    - [Play Store](https://play.google.com/store/apps/details?id=ga.testapp.testapp)
    - [F-Droid](https://f-droid.org/en/packages/ga.testapp.testapp/)
* iOS: [App Store](https://apps.apple.com/us/app/testapp-system/id1490425513)
* Desktop:
    - [Website](https://testapp.ga/static/downloads.html)
    - [GitLab](https://gitlab.com/testapp-system/testapp-electron/pipelines)
    - Linux: [Snap Store](https://snapcraft.io/testapp-desktop)
* Chrome OS: [Web Store](https://chrome.google.com/webstore/detail/testapp-system/hclopnbfffconajgdcmibjekjhmfegjf)

### Technische Ziele

* **Statistiken** f&uuml;r Sch&uuml;ler\*innen und Lehrer\*innen
* **Motivierende** Benutzererfahrung
* Auf allen Ger&auml;ten verf&uuml;gbar
* **Mobil**-Apps, **Desktop**-Version and **Webseite**
* **Sicher** und **Datenschutzkonform**
* *100% Open-Source*
* Wissenschaftliche Aufgaben mit **LaTex** und **RegEx**
* **Vokabeltests** mit verschiedenen Antwortm&ouml;glichkeiten
* **Ankreuz-/Multiple-Choice-Aufgaben**
* **Random Tests** und **Join Tests**

### Die Entwicklung unterst&uuml;tzen

Du kannst uns durch programmieren helfen. Einfach um Zugriff auf unsere [GitLab TestApp Gruppe](https://gitlab.com/testapp-system) erfragen.

*Unsere Server und Mobil-Apps verteilen sich nicht von selbst! Unterst&uuml;tze uns durch eine Spende!*

Spende gerne: [Buy me a coffee](https://www.buymeacoffee.com/JasMich) oder BTC: *3NUiJXDCkyRTb9Tg7n63yK6Y7CexADtSEh*

## Lizenz

Die TestApp ist unter den Bedingungen der `EUPL-1.2` lizenziert.
