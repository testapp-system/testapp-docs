# Registrieren

Sobald du die TestApp augerufen hast, musst du dich registrieren. Das ist notwendig, damit wir wissen, welche Testergebnisse von dir stammen.

W&auml;hle zuerst aus, ob du Lehrer\*in oder Sch&uuml;ler\*in bist.

Gib deinen **vollen Namen** an, damit deine Lehrer wissen, wer du bist.

Deine **Mail-Adresse** ben&ouml;tigst du, damit du dich sp&auml;ter anmelden kannst und wir unseren gesetzlichen Datenschutz-Auskunftspflichten dir gegen&uuml;ber nachkommen k&ouml;nnen.

Dein **Passwort** kannst du selbst w&auml;hlen. Merk es dir gut, damit du beim n&auml;chsten mal noch wei&szlig;t, wie du dich anmelden sollst.

Wenn du **Lehrer\*in** bist, ben&ouml;tigst du einen Voucher. frage dazu bitte eine\*n bereits registriere\*n Lehrer\*in und lies die [Hinweise f&uuml;r Lehrer\*innen](../lehrer/lehrer-registrieren.md).