# Random Tests

Random Tests sind &Uuml;bungsaufgaben. Du kannst ausw&auml;hlen, &uuml;ber welche Themen du schreiben m&ouml;chtest und wie viele Aufgaben du bearbeiten m&ouml;chtest. Danach kriegst du von unseren Algorithmen passende Aufgaben zur&uuml;ck. Wenn du auf `Start test` dr&uuml;ckst, kommst du auf die Seite zum [Test Schreiben](test-schreiben.md).