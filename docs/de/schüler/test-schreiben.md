# Test schreiben

## Aufgabenformate

Wenn du einen Test schreibst, gibt es verschiedene Aufgabenformate. Diese sind:

 - Eingabe mit festgelegter Reihenfolge
 - Eingabe ohne Reihenfolge
 - Einfache Auswahl
 - Mehrfache Auswahl

### Eingabe

Wenn eine Aufgabe mehr als eine Antwort hat, ist es bei manchen Aufgaben wichtig, in welcher Reihenfolge die Antworten genannt werden. Wenn dem so ist, steht das in der Aufgabe mit drin.

### Auswahl

Wenn bei einer Aufgabe nur ein Ergebnis ausgew&auml;hlt werden kann, ist das Auswahlk&auml;stchen rund. Aufgaben, bei denen Mehrere Antworten richtig sind, erkennst du an den eckigen Auswahlk&auml;stchen.

### Aussehen

Alle Aufgaben haben eine &Uuml;berschrift, eine Aufgabenstellung und den Raum f&uuml;r Antworten.

Manche Aufgaben haben zus&auml;tzlich Bilder oder Grafiken, die mit angezeigt werden. Au&szlig;erdem verwenden manche Aufgaben wissenschaftliche Formeln. Diese werden durch [LaTeX](https://latex.org/) dargestellt.

## Test abbrechen

Tests k&ouml;nnen einfach abgebrochen werden, indem zur&uuml;ck navigiert wird oder die TestApp geschlossen wird.

 - Bei Random Tests wird der Test dann verworfen,
 - w&auml;hrend bei Join Tests der Test trotzdem gespeichert wird.

## Datenschutz

Deine Antworten sind nur dir und den Lehrer\*innen sichtbar, in deren Kursen du bist, die genau die Themen deines Tests bearbeiten. Das hei&szlig;t, deine Chemielehrerin kann nicht deine Ergebnisse auf einem Deutschtest einsehen.

## Abspeichern

Wenn du den Test abschickst, dauert es einen Moment, bis wir dein Ergebnis berechnet haben. Sobald wir damit fertig sind, kommst du zur [Test Score](test-score.md) Seite.

!!! Info
    **Bitte beachte**: Das Abspeichern kann abh&auml;ngig von deiner Internetverbindung manchmal etwas l&auml;nger dauern.