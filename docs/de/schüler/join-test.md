# Join Tests

Join Tests sind Tests, die ein Kurs zeitgleich schreibt, &auml;hnlich, wie analoge Tests oder Klausuren in der Schule.

## Einem Test beitreten

Wenn dein Kurs einen Join Test schreiben m&ouml;chte, bekommen alle Sch&uuml;ler einen Zuganscode, die **Join Id**.

Die Join Id muss in das Eingabefeld eingetragen werden, damit der Join Test gestartet werden kann. Wenn du den Test gestartet hast, gelangst du zum [Test Schreiben](test-schreiben.md).

## Zeitlimit und Abbruch

Wenn du dich bei einem Join Test entscheidest, diesen zwischendurch abzubrechen, wird der Test - im Gegensatz zum Random Test - trotzdem abgespeichert.

Au&szlig;erdem haben Join Tests einen Timer, nach dem sie automatisch abgeschickt werden. Dieser wird in der rechten oberen Ecke des Bildschirms angezeigt.