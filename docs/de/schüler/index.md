# Die TestApp als Sch&uuml;ler\*in

Wenn du die TestApp als Sch&uuml;ler\*in aufrufst, siehst du das [Dashboard](dashboard.md). An der Seite befindet sich ein Navigationsmenü mit folgenden Optionen:

 - [Dashboard](dashboard.md)
 - [Random Test](random-test.md)
 - [Join Test](join-test.md)
 - [Dein Score](score-schüler.md)
 - [&Uuml;ber](../allgemein/about.md)
 - [Einstellungen](../allgemein/einstellungen.md)